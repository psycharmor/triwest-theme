<?php
/*
    Name: learndash_custom_functions.php
    Description:
        All functions that are customized versions of learndash functions
        tailored to our needs or any additional actions related to learndash
*/

/* show custom field box for courses */
function pai_add_meta_boxes() {
    add_meta_box(
    	'postcustom',
    	__( 'Custom Fields' ),
    	'post_custom_meta_box',
    	'sfwd-courses'
    );
}
add_action( "add_meta_boxes", "pai_add_meta_boxes" );

/* learndash redirection after course completion */
function pai_course_complete_redirect( $link, $course_id ) {
    /*
        @params:
            $link           -> string
            $course_id      -> int
        @return:
            string
    */

    return home_url( "/my-dashboard/" );
}
add_filter( "learndash_course_completion_url", "pai_course_complete_redirect", 5, 2 );

function pai_get_course_certificate_link( $course_id, $cert_user_id = null )
{
	$cert_user_id = ! empty( $cert_user_id ) ? intval( $cert_user_id ) : get_current_user_id();

	//if ( ( empty( $course_id ) ) || ( empty( $cert_user_id ) ) || ( !sfwd_lms_has_access( $course_id, $cert_user_id ) ) ) {
	if ( ( empty( $course_id ) ) || ( empty( $cert_user_id ) ) ) {
		return '';
	}

	$certificate_id = learndash_get_setting( $course_id, 'certificate' );
	if ( empty( $certificate_id ) ) {
		return '';
	}

	if ( ( learndash_get_post_type_slug( 'certificate' ) !== get_post_type( $certificate_id ) ) ) {
		return '';
	}

	if ( ( learndash_get_post_type_slug( 'course' ) !== get_post_type( $course_id ) ) ) {
		return '';
	}


	$course_status = learndash_course_status( $course_id, $cert_user_id );
	if ( $course_status != esc_html__( 'Completed', 'learndash' ) ) {
		return '';
	}

	$view_user_id = $cert_user_id;

	$cert_query_args = array(
		"course_id"	=>	$course_id,
	);

	// We add the user query string key/value if the viewing user is an admin. This
	// allows the admin to view other user's certificated
	$cert_query_args['user'] = $cert_user_id;
	$cert_query_args['cert-nonce'] = wp_create_nonce( $course_id . $cert_user_id . $view_user_id);

	$url = add_query_arg( $cert_query_args, get_permalink( $certificate_id ) );
	return apply_filters('learndash_course_certificate_link', $url, $course_id, $cert_user_id);
}

/* Allow any user to view anyone's certificate */
function pai_template_redirect_access() {
	global $wp;
	global $post;

	if ( empty( $post ) ) {
		return;
	}

	if ( ! ( $post instanceof WP_Post ) ) {
		return;
	}

	if ( get_query_var( 'post_type' ) ) {
		$post_type = get_query_var( 'post_type' );
	} else {
		if ( ! empty( $post ) ) {
			$post_type = $post->post_type;
		}
	}

	if ( empty( $post_type ) ) {
		return;
	}

	if ( is_robots() ) {
		do_action( 'do_robots' );
	} elseif ( is_trackback() ) {
		include ABSPATH . 'wp-trackback.php';
	} elseif ( ! empty( $wp->query_vars['name'] ) ) {
		if ( ( 'sfwd-quiz' === $post_type ) || ( 'sfwd-lessons' === $post_type ) || ( 'sfwd-topic' === $post_type ) ) {
			global $post;
			sfwd_lms_access_redirect( $post->ID );
		}
	}

	if ( ( 'sfwd-certificates' === $post_type ) ) {

		if ( ( isset( $_GET['course_id'] ) ) && ( ! empty( $_GET['course_id'] ) ) ) {
			$course_id = intval( $_GET['course_id'] );

			$cert_user_id = intval( $_GET['user'] );
			$view_user_id = $cert_user_id;

			if ( ( isset( $_GET['cert-nonce'] ) ) && ( ! empty( $_GET['cert-nonce'] ) ) ) {
				$course_status = learndash_course_status( $course_id, $cert_user_id );
				if ( esc_html__( 'Completed', 'learndash' ) === $course_status ) {

					wp_set_current_user( $cert_user_id );

					/**
					 * Include library to generate PDF
					 */
					require_once (WP_PLUGIN_DIR . '/sfwd-lms/includes/ld-convert-post-pdf.php');
					post2pdf_conv_post_to_pdf();
					die(); // 1-800-273-8255
				}
			}
		}
	}
}
function pai_template_redirect() {
	add_action( "template_redirect", "pai_template_redirect_access", 2 );
}
add_action( "init", "pai_template_redirect", 2 );

function send_certificate_email( $data ) {
	/*
		Send a email containing the certificate link to the user if a certificate
		course is completed
		@params:
			@data				-> array
		@return:
			None
	*/

	$course = $data["course"]->post_title;
	$link = pai_get_course_certificate_link( $data["course"]->ID, $data["user"]->data->ID );

	if ( !empty( $link ) ) {
        $survey = "https://www.surveymonkey.com/r/JF7P3LM";
        $header = home_url( "/wp-content/uploads/2019/08/Email-Header-Final.png" );
        $footer = home_url( "/wp-content/uploads/2019/08/Email-Footer-Final.png" );

        $headers = array( "Content-Type: text/html; charset=UTF-8" );
        $msg = "<img style='display: block; width: 100%;' src='$header' alt='TriWest header'><br><br>";
		$msg .= "Congratulations! You earned a certificate for completing the VA-required training for providers. You can download your certificate with the link below:<br><br>";
		$msg .= "<a href='$link' target='_blank'>$link</a><br><br>";
        $msg .= "While you’re here, we’d like your feedback on how you heard about the VA training. Would you take less than a minute to answer ONE question by clicking below?<br><br>";
		$msg .= "<a href='$survey' target='_blank'>$survey</a><br><br>";
        $msg .= "Veterans sacrificed and trained to serve us. Thank you for returning the favor by training to serve them.<br><br>";
		$msg .= "SIGNATURE<br><br>";
        $msg .= "<img style='display: block; width: 100%;' src='$footer' alt='TriWest footer'>";
		wp_mail($data['user']->data->user_email, "Download Your Certificate for the VA Training!", $msg, $headers);
	}
}
add_action( "learndash_course_completed", "send_certificate_email", 5, 1 );
