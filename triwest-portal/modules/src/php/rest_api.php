<?php
/*
    Name: rest_api.php
    Description:
        Registers APIs created by the portal and includes other functions, hooks,
        and filters related to API handling
*/

include_once ( get_stylesheet_directory() . "/modules/triwest-portal-api/src/php/triwest_api_controller.php" );


/* replace /wp-json/ with /api/ */
function triwest_portal_api_slug( $slug ) {
    /*
        @params:
            $slug       -> string
        @return:
            string
    */

    return "api";
}
add_filter( "rest_url_prefix", "triwest_portal_api_slug" );


/* blacklist every endpoint except for a select few */
function blacklist_api_endpoints( $endpoints ) {
    /*
        @params:
            $endpoints      -> array
        @return:
            array
    */
    $whitelisted_prefixes = array(
        "triwest",
        "jwt-auth",
        "ldlms"
    );

    foreach( $endpoints as $endpoint => $details ) {
        $whitelisted = false;
        foreach( $whitelisted_prefixes as $prefix ) {
            if( fnmatch( "/" . $prefix . "/*", $endpoint, FNM_CASEFOLD ) ) {
                $whitelisted = true;
            }
        }

        if( $whitelisted === false ) {
            unset( $endpoints[$endpoint] );
        }
    }

    return $endpoints;
}
add_filter( "rest_endpoints", "blacklist_api_endpoints" );

/* extend the jwt-auth token to a certain amount */
function extend_jwt_auth_expire() {
	$days = 365;
	return time() + (DAY_IN_SECONDS * $days);
}
add_action( "jwt_auth_expire", "extend_jwt_auth_expire" );


/* Register the TriWest API Controller */
function register_triwest_api_controller() {
    /*
        Create the TriWest API Controlle object and call the method that
        registers the controller's routes
        @params:
            None
        @return:
            None
    */

    $controller = new Triwest_Api_Controller();
    $controller->register_routes();
}
add_action( "rest_api_init", "register_triwest_api_controller" );
