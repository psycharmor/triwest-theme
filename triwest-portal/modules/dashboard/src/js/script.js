"use strict";

/*
    The main script file of the dashboard page used to redirect users to the
    courses that they have clicked
*/

var $j = jQuery.noConflict();

$j( document ).ready( function() {
    /*
        Handles all the click events related to the dashboard
    */

    $j( ".pai-dashboard-course-box" ).click( function() {
        var id = $j( this ).attr( "id" ).substring( 7 );
        enroll_and_link_to_course( id );
    });
});

function enroll_and_link_to_course( id ) {
    /*
        Make an ajax call to send the user to the course, enrolling them first
        if they are not enrolled yet.
        @params:
            id          -> string
        @return:
            None
    */

    $j( "body" ).css( "cursor", "wait" );

    var data = {
        "action":       "triwest_portal_enroll_user",
        "course_id":    id
    };

    $j.ajax({
        type:       "POST",
        url:        window.location.origin + "/wp-admin/admin-ajax.php",
        data:       data,
        timeout:    300000,
        success:    function( response ) {
            $j( "body" ).css( "cursor", "" );
            var url = $j.parseJSON(response);
            window.open( url["url"], url["target"] );
        },

        error:      function() {
            $j( "body" ).css( "cursor", "" );
            console.log( "enroll_and_link_to_course: Cannot connect to ajax" );
        }
    });
}
