<?php
/*
    Name: dashboard_page_template.php
    Description:
        Extend the base page template to create the dashboard page
*/

namespace triwest_portal;

include_once ( get_stylesheet_directory() . "/modules/dashboard/src/php/profile.php" );
include_once ( get_stylesheet_directory() . "/modules/dashboard/src/php/certificates.php" );
include_once ( get_stylesheet_directory() . "/modules/dashboard/src/php/courses.php" );
include_once ( get_stylesheet_directory() . "/modules/src/php/page_template.php" );

class Dashboard_Page_Template extends Page_Template {

    private $user;
    private $profile;
    private $certificates;
    private $courses;

    public function __construct() {
        /*
            Checks if the user is logged in first before continuing.
            Sets the class member variables
        */

        if( !is_user_logged_in() ) {
            wp_safe_redirect( wp_login_url() );
            exit;
        }

        $this->user = wp_get_current_user();
        $this->profile = new Dashboard_Profile( $this->user );
        $this->certificates = new Dashboard_Certificates( $this->user );
        $this->courses = new Dashboard_Courses( $this->user );
    }

    private function include_files() {
        /*
            Include all the stylesheets and scripts needed for the Dashboard page
            @params:
                None
            @return:
                None
        */

        $content = "<!-- FROM THE DASHBOARD TEMPLATE -->";

        $content .= "<script type='text/javascript' src='" . get_stylesheet_directory_uri() . "/modules/dashboard/src/js/script.js'></script>";
        $content .= "<link type='text/css' rel='stylesheet' href='" . get_stylesheet_directory_uri() . "/modules/dashboard/src/css/style.css'>";

        return $content;
    }

    private function create_head_element() {
        /*
            Create the <head> element
            @params:
                None
            @return:
                string
        */

        $title = "My Dashboard - " . get_bloginfo( "name" );

        $content = "";

        $content .= "<head>";
            $content .= "<title>$title</title>";
            $content .= $this->include_base_files();
            $content .= $this->include_files();
        $content .= "</head>";

        return $content;
    }

    private function create_dashboard_header_element() {
        /*
            Create the blue header element located at the bottom of the logo
            @params:
                None
            @return:
                string
        */

        $content = "";

        $content .= "<div class='pai-dashboard-header'>";
            $content .= "<a href='" . wp_logout_url() . "'>Log Out</a>";
        $content .= "</div>"; //pai-dashboard-header

        return $content;
    }

    private function create_registration_completed_msg() {
        /*
            Show that the registration msg has been completed if
            the user logs in for the first time and delete the meta
            @params:
                None
            @return:
                string
        */

        $user_just_registered = get_user_meta( $this->user->ID, "registration_completed", true );

        if( $user_just_registered ) {
            $title = "Registration Completed!";
            $subtitle = "You may now take your training below. Please note that the VA-required training is listed at the top. All other courses are optional and offered as supplemental training to better serve Veterans.";

            $content = "";

            $content .= "<div class='pai-dashboard-registration-completed'>";
                $content .= "<h1>$title</h1>";
                $content .= "<h2>$subtitle</h2>";
            $content .= "</div>"; // pai-dashboard-registration-completed

            delete_user_meta( $this->user->ID, "registration_completed" );

            return $content;
        }
    }

    private function create_left_side() {
        /*
            Create the left side of the page which consists of the following:
                User Profile
                Certificates earned
            @params:
                None
            @return:
                string
        */

        $content = "";

        $content .= "<div class='col-12 col-lg-4 pai-dashboard-left-side-container'>";
            $content .= $this->profile->create_section();
            $content .= $this->certificates->create_section();
        $content .= "</div>"; // pai-dashboard-left-side-container

        return $content;
    }

    private function create_right_side() {
        /*
            Create the right side of the page which consists of the following:
                courses
            @params:
                None
            @return:
                string
        */

        $content = "";

        $content .= "<div class='col-12 col-lg-6 pai-dashboard-right-side-container'>";
            $content .= $this->courses->create_section();
        $content .= "</div>"; // pai-dashboard-right-side-container

        return $content;
    }

    private function create_body_element() {
        /*
            Create the <body> element
            @params:
                None
            @return:
                string
        */

        $content = "";

        $content .= "<body class='pai-bg-img'>";
            $content .= "<div class='pai-dashboard-container'>";
                $content .= $this->create_header();
                $content .= $this->create_header_img();
                $content .= $this->create_dashboard_header_element();
                $content .= $this->create_registration_completed_msg();
                $content .= "<div class='pai-dashboard-container-content'>";
                    $content .= $this->create_left_side();
                    $content .= $this->create_right_side();
                $content .= "</div>"; // pai-dashboard-container-content
            $content .= "</div>"; // pai-dashboard-container
            $content .= $this->create_footer();
        $content .= "</body>";

        return $content;
    }

    public function create_page() {
        /*
            Create the entire page, including the <head> and the <body>.
            @params:
                None
            @return:
                None
        */

        $content = "";

        $content .= $this->create_head_element();
        $content .= $this->create_body_element();

        echo html_entity_decode( $content );
    }
}
