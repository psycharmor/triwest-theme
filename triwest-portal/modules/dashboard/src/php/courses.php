<?php
/*
    Name: courses.php
    Description:
        Create the class responsible for creating the dashboard's course lists
*/

namespace triwest_portal;

class Dashboard_Courses {

    private $user;

    public function __construct( $user ) {
        /*
            Set the class's member variables
            @params:
                $user           -> WP_User
        */

        $this->user = $user;
    }

    private function get_all_courses() {
        /*
            Get all courses from the database
            @params:
                None
            @return:
                array( Course )
        */

        $args = array(
            "post_type" => "sfwd-courses",
            "post_status" => "publish",
            "nopaging" => true,
            "order" => "ASC"
        );

        $query = new \WP_Query( $args );

        return $query->posts;
    }

    private function has_course_tag( $course_id, $tag ) {
        /*
            check whether the course has the course tag or not
            @params:
                $course_id          -> int
                $tag                -> string
            @return:
                bool
        */

        $terms = wp_get_post_terms( $course_id, "ld_course_tag" );

        // no course tags associated with the course
        if( empty( $terms ) ) {
            return false;
        }

        // need to find out if the course contains the course tag
        foreach( $terms as $term ) {
            if( $term->name === $tag ) {
                return true;
            }
        }

        // couldn't find the course tag
        return false;
    }

    private function find_courses_by_course_tag( $tag, $exclude ) {
        /*
            get all courses that either have the course_tag or don't
            @params:
                $tag            -> string
                $exclude        -> bool
            @return:
                array( int => string )
        */

        $filtered_courses = array();
        $courses = $this->get_all_courses();
        foreach( $courses as $course ) {
            $course_id = $course->ID;
            if( $exclude ) {
                if( !$this->has_course_tag( $course_id, $tag ) ) {
                    $filtered_courses[$course_id] = get_the_title( $course_id );
                }
            }

            else if( !$exclude ) {
                if( $this->has_course_tag( $course_id, $tag ) ) {
                    $filtered_courses[$course_id] = get_the_title( $course_id );
                }
            }
        }

        return $filtered_courses;

    }

    private function create_course_box( $id, $title ) {
        /*
            Create the container that holds the course and link to the course
            @params:
                $id             -> int
                $title          -> string
            @return:
                string
        */
        $complete = "";
        if( learndash_course_status( $id, $this->user->ID ) == esc_html__( 'Completed', 'learndash' ) ) {
            $complete = "complete";
        }
        $id = "course-" . $id;

        $content = "";

        $content .= "<div class='pai-dashboard-course-box' id='$id'>";
            $content .= "<div class='pai-dashboard-course-box-btn-container play'>";
                $content .= "<i class='fa fa-play-circle-o pai-dashboard-play-btn'></i>";
            $content .= "</div>"; // pai-dashboard-course-box-btn-container
            $content .= "<p class='pai-dashboard-text-title'>$title</p>";
            $content .= "<div class='pai-dashboard-course-box-btn-container check $complete'>";
                $content .= "<i class='fa fa-check-circle pai-dashboard-check-btn'></i>";
            $content .= "</div>"; // pai-dashboard-course-box-btn-container check
        $content .= "</div>"; // pai-dashboard-course-box

        return $content;
    }

    private function create_course_list( $courses, $title, $special ) {
        /*
            Create a list of course boxes. Display depends on whether they are
            special courses
            @params:
                $courses            -> array( int => string )
                $title              -> string
                $special            -> bool
            @return:
                string
        */

        $sp = "";
        $red_title = "";

        if( $special ) {
            $sp = "special";
            $red_title = "red";
        }

        $content = "";
        $content .= "<div class='pai-dashboard-course-list $sp'>";
            $content .= "<h2 class='pai-dashboard-course-group-title $red_title'>$title</h2>";
            foreach( $courses as $id => $title ) {
                $content .= $this->create_course_box( $id, $title );
            }
        $content .= "</div>"; // pai-dashboard-course-list

        return $content;
    }

    public function create_section() {
        /*
            Create the course section of the dashboard
            @params:
                None
            @return:
                string
        */

        $train_courses = $this->find_courses_by_course_tag( "train_course", false );
        $supplement_courses = $this->find_courses_by_course_tag( "train_course", true );

        $content = "";
        $content .= $this->create_course_list( $train_courses, "VA-required Training Status", true );
        $content .= $this->create_course_list( $supplement_courses, "Supplemental Provider TriWest Training", false );

        return $content;
    }
}
