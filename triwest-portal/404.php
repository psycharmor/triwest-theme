<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */


?>

<section id="primary" class="content-area">
    <main id="main" class="site-main">

        <div class="error-404 not-found">
            <header class="page-header">
                <h1 class="page-title"><?php _e( 'Error 404: Oops! That page can&rsquo;t be found.', 'twentynineteen' ); ?></h1>
            </header><!-- .page-header -->
        </div><!-- .error-404 -->

    </main><!-- #main -->
</section><!-- #primary -->
