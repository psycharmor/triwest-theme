<?php
/*
    Template Name: Dashboard Template
*/

include_once ( get_stylesheet_directory() . "/modules/dashboard/src/dashboard_page_template.php" );

$dashboard = new \triwest_portal\Dashboard_Page_Template();
$dashboard->create_page();
